import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoufPage } from './logouf.page';

describe('LogoufPage', () => {
  let component: LogoufPage;
  let fixture: ComponentFixture<LogoufPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoufPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoufPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
