import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  nome = 'Nome'
  telefone = 'Telefone'
  cep = 'CEP'
  endereco = 'Endereço (rua e número)'
  bairro = 'Bairro'
  complemento = 'Complemento'
  usuario = 'Usuário'
  senha = 'Senha'

  constructor() { }

  ngOnInit() {
  }

}
